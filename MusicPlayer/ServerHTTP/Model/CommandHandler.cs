﻿using System.Collections.Generic;
using ServerHTTP.Commands;

namespace ServerHTTP.Model
{
    class CommandHandler
    {
        private Dictionary<string, ICommandBehavior> commands 
            = new Dictionary<string, ICommandBehavior>
                {
                    {"list", new ShowerCommandBehavior() },
                    {"search", new SearcherCommandBehavior() },
                    {"add", new AdderCommandBehavior() },
                    {"del", new DeleterCommandBehavior() },
                    {"save", new SaverCommandBehavior() },
                    {"load1", new LoaderStartCommandBehavior() },
                    {"load2", new LoaderEndCommandBehavior() },
                };

        internal string Handle(TransferData transferData,ref List<Song> playList)
        {
            return commands[transferData.CommandName].ExecuteAction(ref playList, transferData);
        }
    }
}
