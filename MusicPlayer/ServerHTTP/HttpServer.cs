﻿using System;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.IO;
using ServerHTTP.Model;
using System.Text;

namespace ServerHTTP
{
    public class HttpServer
    {
        public const String VERSION = "HTTP/1.1";
        public const String NAME = "Music HTTP Server v0.1";

        private bool running = false;

        private TcpListener listener;

        public HttpServer(int port)
        {
            listener = new TcpListener(IPAddress.Any, port);
        }

        public void Start()
        {
            Thread serverThread = new Thread(Run);
            serverThread.Start();
        }
        
        private void Run()
        {
            int nWorkerThreads = 5, nCompletionThreads = 5;

            ThreadPool.GetMaxThreads(out nWorkerThreads, out nCompletionThreads);

            running = true;
            listener.Start();

            while (running)
            {
                WriteLine("Wating for connection...");

                ThreadPool.QueueUserWorkItem(new WaitCallback(InteractionWithClient), listener.AcceptTcpClient());
            }

            running = false;

            listener.Stop();
        }

        private void InteractionWithClient(object state)
        {
            TcpClient client = state as TcpClient;

            WriteLine("Client connected!");

            if (client != null)
            {
                HandleClient(client);
                client.Close();
            }
        }

        private void HandleClient(TcpClient client)
        {
            Thread.Sleep(500);
            int lenghtOfStream = client.Available;
            StreamReader reader = new StreamReader(client.GetStream());

            StringBuilder msg = new StringBuilder(); 

            for (int i = 0; i < lenghtOfStream; i++)
            {
                msg.Append(Char.ConvertFromUtf32(reader.Read()));
            }

            RequestHandler requestHandler = new RequestHandler();

            TransferData request = requestHandler.GetRequest(msg.ToString());

            ClientHandler clientHandler = ClientHandler.Instance;
            
            string responseString = clientHandler.HandleClient(request);

            requestHandler.SendResponse(responseString, client.GetStream());
        }

        public static void WriteLine(string requestMsg)
        {
            Console.WriteLine(requestMsg);
        }
    }
}
