﻿using System;
using System.IO;
using System.Collections.Generic;
using ServerHTTP.Model;

namespace ServerHTTP.Commands
{
    class SaverCommandBehavior : ICommandBehavior
    {
        public string ExecuteAction(ref List<Song> songs, TransferData transferData)
        {
            string resaultString = null;
            if (!transferData.FileName.Contains("/")
                && !transferData.FileName.Contains("\\")
                && transferData.FileName.Contains(".pls"))
            {
                try
                {
                    SaveToPls(transferData.FileName, songs);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    resaultString = "Success!";
                }
            }
            else
            {
                resaultString = "It was entered incorrect value!";
            }

            return resaultString;
        }

        private void SaveToPls(string fileName, List<Song> songs)
        {
            FileStream file = new FileStream(fileName, FileMode.Create);

            using (StreamWriter writer = new StreamWriter(file))
            {
                writer.WriteLine("[playlist]\nNumberOfEntries={0}", songs.Count);

                for(int i = 0; i < songs.Count; i++)
                {
                    writer.WriteLine($"File{i}={songs[i].Reference}");
                    writer.WriteLine($"Title{i}={songs[i].Author} - {songs[i].Name}");
                    writer.WriteLine($"Length{i}={songs[i].Length}\n");
                }
            }
        }
    }
}
