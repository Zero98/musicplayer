﻿using System.IO;
using System.Collections.Generic;
using ServerHTTP.Model;

namespace ServerHTTP.Commands
{
    class LoaderStartCommandBehavior : ICommandBehavior
    {
        public string ExecuteAction(ref List<Song> songs, TransferData transferData)
        {
            string[] filesname = Directory.GetFiles(Directory.GetCurrentDirectory(), "*.pls");

            return GetFileList(filesname);
        }
        
        private string GetFileList(string[] filesname)
        {
            string fileString = "";

            for (int i = 0; i < filesname.Length; i++)
            {
                fileString += $"{filesname[i]}\n";
            }
            
            return fileString.Trim('\n');
        }
    }
}