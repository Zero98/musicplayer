﻿using System.Collections.Generic;
using ServerHTTP.Model;

namespace ServerHTTP.Commands
{
    class AdderCommandBehavior : ICommandBehavior
    {
        public string ExecuteAction(ref List<Song> songs, TransferData transferData)
        {
            songs.Add(transferData.Song);
            return "Success!";
        }   
    }
}
