﻿using System.Collections.Generic;
using ServerHTTP.Model;

namespace ServerHTTP.Commands
{
    interface ICommandBehavior
    {
        string ExecuteAction(ref List<Song> songs, TransferData transferData);
    }
}
