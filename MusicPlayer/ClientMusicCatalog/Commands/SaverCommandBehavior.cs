﻿using ClientMusicCatalog.Model;

namespace ClientMusicCatalog.Commands
{
    class SaverCommandBehavior : ICommandBehavior
    {
        /// <summary>
        /// Save the collection of songs in the pls file
        /// </summary>
        public void ExecuteAction()
        {
            var fileName = ClientMenu.GetField("Enter the name of the file");
            
            Song nulladbleSong = new Song(null, null, null);

            var transferData = new TransferData(nulladbleSong, "save") { FileName = fileName};

            Connection connection = Connection.Instance;

            string receivedData =  connection.MakePostRequestToServer(transferData);
            ClientMenu.WriteLine(receivedData);
        }
    }
}
