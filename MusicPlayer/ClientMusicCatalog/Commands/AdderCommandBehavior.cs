﻿using ClientMusicCatalog.Model;

namespace ClientMusicCatalog.Commands
{
    class AdderCommandBehavior : ICommandBehavior
    {
        /// <summary>
        /// Compiles a song object and then sends it to the server to add
        /// </summary>
        public void ExecuteAction()
        {
            Song song = new Song(
                ClientMenu.GetField("Input author's name:"),
                ClientMenu.GetField("Input the composition's name:"),
                ClientMenu.GetField("Input the reference:"));

            if (song.IsCorrectSong())
            {
                TransferData transferData = new TransferData(song, "add");

                Connection connection = Connection.Instance;
                string receivedData = connection.MakePostRequestToServer(transferData);
                ClientMenu.WriteLine(receivedData);
            }
            else
            {
                ClientMenu.WriteLine("Incorrect data");
            }
        }
    }
}
