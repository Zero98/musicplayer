﻿using ClientMusicCatalog.Model;

namespace ClientMusicCatalog.Commands
{
    class SearcherCommandBehavior : ICommandBehavior
    {
        /// <summary>
        /// Search by collection
        /// </summary>
        public void ExecuteAction()
        {
            string query = ClientMenu.GetField("Input the part of the name to find composition in the catalog:");

            Song removableSong = new Song(query, query, null);

            TransferData transferData = new TransferData(removableSong, "search");

            Connection connection = Connection.Instance;

            string receivedData = connection.MakePostRequestToServer(transferData);

            ClientMenu.WriteLine(receivedData);
        }
    }
}
