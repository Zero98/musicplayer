﻿using ClientMusicCatalog.Model;

namespace ClientMusicCatalog.Commands
{
    class LoaderCommandBehavior : ICommandBehavior
    {
        /// <summary>
        /// Downloading a collection of songs
        /// </summary>
        public void ExecuteAction()
        {
            ClientMenu.WriteLine("Selecte file");

            Song nulladbleSong = new Song(null, null, null);

            var transferData = new TransferData(nulladbleSong, "load1");

            Connection connection = Connection.Instance;

            string receivedData = connection.MakePostRequestToServer(transferData);

            string[] listOfFiles = receivedData.Split('\n');

            ShowFiles(listOfFiles);

            string selectedFile = ClientMenu.GetField("Enter the number of the selected directory");
            
            int selectedFileInInt;
            if(int.TryParse(selectedFile, out selectedFileInInt) 
                      && selectedFileInInt < listOfFiles.Length
                      && selectedFileInInt >= 0)
            {
                var transferData2 = new TransferData(nulladbleSong, "load2") { FileName = listOfFiles[selectedFileInInt] };
                receivedData = connection.MakePostRequestToServer(transferData2);
                ClientMenu.WriteLine(receivedData);
            }
            else
            {
                ClientMenu.WriteLine("You select incorrect file!");
            }
        }

        private void ShowFiles(string[] listOfFiles)
        {
            for (int i = 0; i < listOfFiles.Length; i++)
            {
                ClientMenu.WriteLine($"{i}. {listOfFiles[i]}");
            }
        }
    }
}