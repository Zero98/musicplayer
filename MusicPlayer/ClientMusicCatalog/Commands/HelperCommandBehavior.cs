﻿namespace ClientMusicCatalog.Commands
{
    class HelperCommandBehavior : ICommandBehavior
    {
        /// <summary>
        /// Reference Information
        /// </summary>
        public void ExecuteAction()
        {
            ClientMenu.WriteLine("Usage:\n" +
            "\tType one of commnands:\n" +
            " \t\t\"list\" to display all items of catalog\n" +
            "\t\t\"search\" to go find items in catalog\n" +
            "\t\t\"add\" to add new item\n" +
            "\t\t\"del\" to remove some item from list\n" +
            "\t\t\"quit\" to exit\n" +
            "\t\t\"save\" to save to the pls-file\n" +
            "\t\t\"load\" to load the file");
        }
    }
}
