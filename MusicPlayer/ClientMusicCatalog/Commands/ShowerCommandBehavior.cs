﻿using ClientMusicCatalog.Model;

namespace ClientMusicCatalog.Commands
{
    class ShowerCommandBehavior : ICommandBehavior
    {
        /// <summary>
        /// Output of the entire collection
        /// </summary>
        public void ExecuteAction()
        {
            

            ClientMenu.WriteLine("All compositions in catalog");

            Song nulladbleSong = new Song(null, null, null);

            var transferData = new TransferData(nulladbleSong, "list");

            Connection connection = Connection.Instance;

            string receivedData =  connection.MakePostRequestToServer(transferData);

            ClientMenu.WriteLine(receivedData);
        }
    }
}
