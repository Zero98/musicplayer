﻿using ClientMusicCatalog.Model;

namespace ClientMusicCatalog.Commands
{
    class DeleterCommandBehavior : ICommandBehavior
    {
        /// <summary>
        /// Compiles a song object and sends it to the server to delete
        /// </summary>
        public void ExecuteAction()
        {
            Song removableSong;
            string query = ClientMenu.GetField("Input the full name of the track to remove:");

            if (query.Contains(" - "))
            {
                removableSong = new Song(query.Split('-')[0], query.Split('-')[1], null);
            }
            else
            {
                removableSong = new Song(query, query, null);
            }
            
            TransferData transferData = new TransferData(removableSong, "del");

            Connection connection = Connection.Instance;

            string receivedData = connection.MakePostRequestToServer(transferData);

            ClientMenu.WriteLine(receivedData);
        }
    }
}
