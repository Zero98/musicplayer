﻿using System;
using ClientMusicCatalog.Model;
using ClientMusicCatalog.Commands;

namespace ClientMusicCatalog
{
    class ClientMenu
    {
        private ICommandBehavior commandBehavior;

        /// <summary>
        ///Activate the user's interface 
        /// </summary>
        public void RunClient()
        {
            CommandList commandList = new CommandList();

            commandList.CallCommand("help");

            do
            {
                Console.WriteLine("Input the command:");

                string command = Console.ReadLine().ToLower();

                if (commandList.СontainCommand(command))
                {
                    commandBehavior = commandList.CallCommand(command);
                }
                else
                {
                    Console.WriteLine("Incorrect command!");
                }
                Console.WriteLine("----");
            } while (!(commandBehavior is ExitCommandBehavior));
        }

        public static void WriteLine(string requestMsg)
        {
            Console.WriteLine(requestMsg);
        }

        public static string GetField(string requestMsg)
        {
            Console.WriteLine(requestMsg);

            return Console.ReadLine();
        }
    }
}
