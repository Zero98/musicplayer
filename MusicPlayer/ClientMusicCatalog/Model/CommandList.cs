﻿using System.Collections.Generic;
using ClientMusicCatalog.Commands;

namespace ClientMusicCatalog.Model
{
    class CommandList
    {
        private Dictionary<string, ICommandBehavior> commands;

        public CommandList()
        {
            commands = new Dictionary<string, ICommandBehavior>
                {
                    {"list", new ShowerCommandBehavior() },
                    {"search", new SearcherCommandBehavior() },
                    {"add", new AdderCommandBehavior() },
                    {"quit", new ExitCommandBehavior() },
                    {"del", new DeleterCommandBehavior() },
                    {"save", new SaverCommandBehavior() },
                    {"load", new LoaderCommandBehavior() },
                    {"help", new HelperCommandBehavior() }
                };
        }

        public ICommandBehavior CallCommand(string command)
        {
            ICommandBehavior executableCommand = commands[command];

            executableCommand.ExecuteAction();

            return executableCommand;
        }

        public bool СontainCommand(string command)
        {
            return commands.ContainsKey(command);
        }
    }
}
