﻿using System;
using System.Text;
using System.Net.Sockets;
using System.Text.RegularExpressions;

namespace ClientMusicCatalog.Model 
{
    class Connection : IDisposable
    {
        private int userId;
        private string serverURL;
        private int serverPort;

        private Socket socket;

        private static Connection connection = null;
        public static Connection Instance
        {
            get
            {
                return (connection != null) ? connection : null;
            }
            private set
            {
                connection = value;
            }
        }

        private Connection(string url, int port, int _userId)
        {
            userId = _userId;
            serverURL = url;
            serverPort = port;
        }

        public string MakePostRequestToServer(TransferData transferData)
        {
            string respouseString = default(string);

            transferData.UserID = userId;
            string transmittedData = TransferData.ConvertToXML(transferData);
            using (socket = Connect())
            {
                if (socket != null)
                {
                    string header = GetPostHeader(transmittedData.Length);
                    Byte[] bytesSent = Encoding.UTF8.GetBytes(header + transmittedData);

                    socket.Send(bytesSent);
                    respouseString = Listen(socket);
                }
            }

            return respouseString.TrimEnd('\0');
        }

        private string Listen(Socket socket)
        {
            Byte[] byteRecived;

            StringBuilder stringBuilder = new StringBuilder();
            int bytes = 0;

            do
            {
                byteRecived = new Byte[1024];
                bytes = socket.Receive(byteRecived, byteRecived.Length, 0);
                stringBuilder.Append(Encoding.UTF8.GetString(byteRecived));
            } while (bytes > 0);
                
            return GetData(stringBuilder.ToString());
        }

        private string GetData(string page)
        {
            Regex regex = new Regex(@"\n\n");

            return regex.Split(page)[1];
        }

        private string GetPostHeader(int contentLenght)
        {
            return "POST / HTTP/1.0\n" +
                $"Host: {serverURL}:{serverPort}\n" +
                $"Content-Length:{contentLenght}\r\n\r\n";
        }

        public static void InitServer(string url,int port)
        {
            if (connection == null)
            {
                Random random = new Random();
                connection = new Connection(url, port, random.Next(0,100));
            }
        }
        
        private Socket Connect()
        {
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                socket.Connect(serverURL, serverPort);
            }
            catch (Exception ex)
            {
                ClientMenu.WriteLine("There isn't connection to the server");
            }

            return socket;
        }

        public void Dispose()
        {
            socket.Shutdown(SocketShutdown.Both);
            socket.Close();
        }
    }
}
